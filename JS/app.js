import {foodService} from './service/foodService.js'
import { loadingService } from './service/loadingService.js';

const $ = document.querySelector.bind(document)
let updateBtn = $('#update');
updateBtn.disabled = true;
let notify = $('.notify');
let foodList = [];
const renderListFood= (list)=>{
    let content = ''
    for(let i =0; i<list.length;i++){
        let food  = list[i]
        let contentBodyTable = 
        `
        <tr>
            <td>${food.id}</td>
            <td>${food.name}</td>
            <td>${food.price}</td>
            <td>${food.description}</td>
            <td>
               <div class="d-flex justify-content-center">
               <button class="btn btn-success mr-2" onclick="editFood(${food.id})"><i class="fas fa-edit"></i></button>
               <button class="btn btn-danger" onclick="deleteFood(${food.id});"><i class="fas fa-trash"></i></button>
               </div>
            </td>
        </tr>
        `
    content += contentBodyTable;
    }
   $('#tbody_food').innerHTML = content
}
//Delete
let deleteFood = (id) => {
    loadingService.onLoading();
    foodService.deleteFood(id)
    .then((res)=>{
        loadingService.offLoading()
        displayNotify("Successful Delete","success")
        renderListService()
    })
    .catch((err)=>{
        loadingService.offLoading()
        displayNotify("Error","danger")
    })
}
window.deleteFood = deleteFood;

// render
let renderListService = ()=>{
    loadingService.onLoading();
    foodService
    .getFoodList()
    .then((res)=>{
        loadingService.offLoading()
        foodList = res.data;
        renderListFood(foodList);
    })
    .catch((err)=>{
        loadingService.offLoading()
        console.log(err)
    })
}
renderListService()

//edit
let editFood = (id) =>{
    loadingService.onLoading();
    updateBtn.disabled = false;
    foodService
    .editFood(id)
    .then((res)=>{
        loadingService.offLoading();

        let foodReturn = res.data;
        $('#id').style.display = 'block'
        $('#id').value = foodReturn.id
        $('#id').disabled = true
        $('#foodName').value = foodReturn.name
        $('#foodPrice').value = foodReturn.price
        $('#description').value = foodReturn.description

        $('#add').disabled = true
    })
    .catch((err)=>{
        loadingService.offLoading();
        displayNotify("Error","danger")
    })
}
window.editFood = editFood;

//update
let updateFoodBtn = () =>{
    loadingService.onLoading();
    let newList = {
        id : '',
        name : '',
        price: '',
        description : '',
    };
    newList.id =  $('#id').value
    newList.name =  $('#foodName').value
    newList.price = $('#foodPrice').value
    newList.description =$('#description').value

    if(newList.name  == ''|| newList.price == '' || newList.description == ''){
        loadingService.offLoading()
        displayNotify("fill full the information","danger")
    }else{
        foodService.updateFood(newList.id,newList)
    .then((res)=>{
        loadingService.offLoading();
        $('#add').disabled = false
        displayNotify("Successful Update","success")
        renderListService()
        resetInput()
        updateBtn.disabled = true;
    })
    .catch((err)=>{
        loadingService.offLoading();
        displayNotify("Error","danger")
    })
    }
}
window.updateFoodBtn = updateFoodBtn;
//ADD
let addFood = () => {
    loadingService.onLoading();
    let food ={
        name : $('#foodName').value ,
        price: $('#foodPrice').value ,
        description :$('#description').value 
    }
   if(food.name == '' || food.price =='' || food.description == ''){
        displayNotify("fill full the information","danger")
        loadingService.offLoading();
   }else{
    foodService.addFood(food)
   .then((res)=>{
        loadingService.offLoading();
        displayNotify("Successful Add","success")
        renderListService();
        resetInput();
   })
   .catch((err)=>{
         loadingService.offLoading();
        displayNotify("Error","danger")
   })
   }
}
window.addFood = addFood;
function resetInput() {
    $('#id').style.display = 'none'
    $('#foodName').value = ''
    $('#foodPrice').value = ''
    $('#description').value = ''
}
function displayNotify(text,action){
    notify.innerText = text;
    notify.classList.add(action);

    setTimeout(function(){
        notify.innerText = '';
        notify.classList.remove(action);   
    },2000)
}