    let BASE_URL =  "https://62b0787ae460b79df0469bbc.mockapi.io/mon-an";
    export let foodService = {

    getFoodList: ()=>{
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },
    deleteFood:(id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method:"DELETE"
        })
    },
    editFood:(id) =>{
        return axios({
            url: `${BASE_URL}/${id}`,
            method:"GET"
        })
    },
    updateFood:(id,newList) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method:"PUT",
            data: newList
        })
    },
    addFood :(food)=>{
        return axios({
            url: BASE_URL,
            method:"POST",
            data: food
        })
    }

}