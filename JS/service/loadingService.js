export let loadingService = {
    onLoading: ()=>{
        document.getElementById('loading').style.display = 'flex'
        document.getElementById('main').style.display = 'none'
    },
    offLoading: ()=>{
        document.getElementById('loading').style.display = 'none'
        document.getElementById('main').style.display = 'block'
    }
}